package controllers

import scalaz._, Scalaz._
import play.api._
import play.api.mvc._
import play.api.libs.json._

import com.atlassian.jsonpatch._


object Application extends Controller {

  implicit val exampleWrites: Writes[Example] = Json.writes[Example]

  case class ExamplePostBody(target: JsValue, patch: JsonPatchDoc)

  implicit val ExamplePostBodyReads: Reads[ExamplePostBody] = Json.reads[ExamplePostBody]

  case class Example(id: String, desc: String, target: JsValue, patch: JsValue) {
    lazy val apply: JsResult[JsValue] = Json.fromJson[JsonPatchDoc](patch).flatMap(_.apply(target))
  }

  val a1: Example = Example("A.1"
                          , "Adding an Object Member"
                          , Json.parse("""{"foo": "bar"} """)
                          , Json.parse("""[{ "op": "add", "path": "/baz", "value": "qux" }]"""))  


  val a2: Example = Example("A.2"
                          , "Adding an Array Element"
                          , Json.parse("""{ "foo": [ "bar", "baz" ] }""")
                          , Json.parse(""" [{ "op": "add", "path": "/foo/1", "value": "qux" }]"""))

  val a3: Example = Example("A.3"
                          , "Removing an Object Member"
                          , Json.parse(
"""{
  "baz": "qux",
  "foo": "bar"
   }""") 
                          , Json.parse(
"""[
  { "op": "remove", "path": "/baz" }
]"""))

  val examples: List[Example] = List(a1, a2, a3)

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  def allExamples = Action {
    Ok(Json.toJson(examples))
  }

  def getExample(id: String) = Action {
    examples.find(_.id === id).map(x => Ok(Json.toJson(x))).getOrElse(NotFound)
  }

  private def applyPatchOrBadRequest(target: JsValue, patch: JsonPatchDoc): Result \/ JsValue = 
    \/.fromEither(patch.apply(target).asEither).leftMap(_ => BadRequest("Failed to apply patch"))

  def patchExample(id: String) = Action(jsonPatchDoc) { request =>
    val res: Result \/ Result = (for {
      target <- scalaz.std.option.toRight(examples.find(_.id === id).map(_.target))(NotFound)
      result <- applyPatchOrBadRequest(target, request.body).map(Ok(_))  
    } yield result)

    res.fold(l = identity, r = identity)
  }

  def postExample = Action(parse.json) { request =>
    val res: Result \/ Result = (for {
      ex      <- \/.fromEither(Json.fromJson[ExamplePostBody](request.body).asEither).leftMap(_ => BadRequest("Invalid request body"))
      patched <- applyPatchOrBadRequest(ex.target, ex.patch).map(Ok(_))
    } yield patched)

    res.fold(l = identity, r = identity)
  }
}