import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "json-patch-sample"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    "json-patch" %% "json-patch" % "1.0-SNAPSHOT",
    "org.scalaz" %% "scalaz-core" % "7.0.0-M9"
  )


  val main = play.Project(appName, appVersion, appDependencies).settings(
  )

}
