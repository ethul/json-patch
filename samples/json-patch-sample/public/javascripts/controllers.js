var renderJson = angular.module("renderJson", []);

renderJson.directive('renderjson', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attr, ngModel) {

            function toJson(text) {
              return JSON.parse(text);
            }

            function toText(json) {
              return JSON.stringify(json);
            }
            ngModel.$parsers.push(toJson);
            ngModel.$formatters.push(toText);
        }
    };
});


function ExampleListCtrl($scope, $http) {
  $http.get('examples').success(function(data) {
    $scope.examples = data;
    $scope.example = {"id": "custom", "desc": "Custom"};
    $scope.examples.unshift($scope.example);
  });

  
  $scope.orderProp = 'desc';

  $scope.submitExample = function(example) {
    $http.post('examples', example).success(function(data) {
      $scope.result = data;
    });
  }
}