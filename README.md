json+patch for Play 2.1
================================

json+patch is a small module for the Play 2.1 framework that supports the draft IETF [JSON patch](http://tools.ietf.org/html/draft-ietf-appsawg-json-patch-10) format.

It provides a series of case classes for deserializing patch documents, transformers for applying patches to existing JSON documents, and a body parser for the `application/json-patch+json` mime type.

Example
-------

```
import com.atlassian.jsonpatch._
import play.api.libs.json._

val json  = Json.parse("""{"foo": "bar"} """)
val patchJson = Json.parse("""[{ "op": "add", "path": "/baz", "value": "qux" }]""")
val result = Json.fromJson[JsonPatchDoc](patch).flatMap(_.apply(json)) // {"foo": "bar", "baz": "qux"}
```

See the sample project for a complete Play application that uses this module.


Known Limitations
-----------------

Patch documents with paths containing array indices are broken. This is due to a [bug in Play](https://github.com/playframework/Play20/issues/943).