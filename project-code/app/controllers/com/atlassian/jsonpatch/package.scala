package com.atlassian

import scalaz._
import scalaz.syntax.std.string._
import scalaz.syntax.std.option._
import play.api._
import play.api.libs.concurrent._
import play.api.libs.json._
import play.api.libs.json.Json
import play.api.libs.iteratee._
import play.api.libs.iteratee.Input._
import play.api.libs.iteratee.Parsing._
import play.api.mvc._
import play.api.data.validation.ValidationError

package object jsonpatch {

  sealed trait JsonPatchPath
  type JsPatchPath = JsPath @@ JsonPatchPath
  def tagPath(p: JsPath): JsPatchPath = Tag[JsPath, JsonPatchPath](p)

  implicit val jsPatchPathReads: Reads[JsPatchPath] = new Reads[JsPatchPath] {
    def next(p: JsPath, s: String) = s.parseInt.toOption.cata(some = i => p(i), none = p \ s)
    def reads(json: JsValue): JsResult[JsPatchPath] = {
      val err = JsError(Seq(JsPath() -> Seq(ValidationError("validate.error.expected.jspath"))))
      json match {
        case JsString(s) => JsSuccess(tagPath(s.split("/").toList.tail.foldLeft((__ : JsPath))(next _)))
        case _ => err
      }      
    }
  }

  implicit val jsonAddReads: Reads[JsonAdd] = Json.reads[JsonAdd]
  implicit val jsonRemoveReads: Reads[JsonRemove] = Json.reads[JsonRemove]
  implicit val jsonReplaceReads: Reads[JsonReplace] = Json.reads[JsonReplace]
  implicit val jsonMoveReads: Reads[JsonMove] = Json.reads[JsonMove]
  implicit val jsonCopyReads: Reads[JsonCopy] = Json.reads[JsonCopy]
  implicit val jsonTestReads: Reads[JsonTest] = Json.reads[JsonTest]
 
  implicit val jsonPatchReads: Reads[JsonPatch] = new Reads[JsonPatch] {
    def reads(json: JsValue): JsResult[JsonPatch] = {
      val err = JsError(Seq(JsPath() -> Seq(ValidationError("validate.error.expected.json.patch"))))
      (__ \ "op").read[String].reads(json).flatMap(typ => typ match {
          case "add" => (__).read[JsonAdd].reads(json)
          case "remove" => (__).read[JsonRemove].reads(json)
          case "replace" => (__).read[JsonReplace].reads(json)
          case "move" => (__).read[JsonMove].reads(json)
          case "copy" => (__).read[JsonCopy].reads(json)
          case "test" => (__).read[JsonTest].reads(json)
          case _ => err
        })
    }
  }

  implicit val jsonPatchDocReads: Reads[JsonPatchDoc] = new Reads[JsonPatchDoc] {
    def reads(json: JsValue): JsResult[JsonPatchDoc] = Reads.list[JsonPatch].reads(json).map(JsonPatchDoc(_))
  }

  def tolerantJsonPatchDoc(maxLength: Int): BodyParser[JsonPatchDoc] = BodyParser("json+patch, maxLength=" + maxLength) { request =>
    def handleError(bytes: Array[Byte], msg: String) = 
      (Play.maybeApplication.map(_.global.onBadRequest(request, msg)).getOrElse(Results.BadRequest), bytes)
    Traversable.takeUpTo[Array[Byte]](maxLength).apply(Iteratee.consume[Array[Byte]]().map( bytes =>
      (for {
        rawJson  <- \/.fromTryCatch[JsValue](Json.parse(new String(bytes, request.charset.getOrElse("utf-8")))).leftMap(_ => handleError(bytes, "Invalid Json"))
        patchDoc <- \/.fromEither(Json.fromJson[JsonPatchDoc](rawJson).asEither).leftMap(_ => handleError(bytes, "Invalid Json+Patch"))
      } yield patchDoc).toEither        
    )).flatMap(Iteratee.eofOrElse(Results.EntityTooLarge))
      .flatMap {
        case Left(b) => Done(Left(b), Empty)
        case Right(it) => it.flatMap {
          case Left((r, in)) => Done(Left(r), El(in))
          case Right(patch) => Done(Right(patch), Empty)
        }
      }
  }

  def tolerantJsonPatchDoc: BodyParser[JsonPatchDoc] = tolerantJsonPatchDoc(play.api.mvc.BodyParsers.parse.DEFAULT_MAX_TEXT_LENGTH)

  def jsonPatchDoc(maxLength: Int): BodyParser[JsonPatchDoc] = BodyParsers.parse.when(
      _.contentType.exists(m => m == "application/json-patch+json"),
      tolerantJsonPatchDoc(maxLength),
      request => Play.maybeApplication.map(_.global.onBadRequest(request, "Expecting application/json+patch body")).getOrElse(Results.BadRequest)
    )

  def jsonPatchDoc: BodyParser[JsonPatchDoc] = jsonPatchDoc(play.api.mvc.BodyParsers.parse.DEFAULT_MAX_TEXT_LENGTH)
}