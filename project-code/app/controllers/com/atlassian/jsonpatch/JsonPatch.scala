package com.atlassian.jsonpatch

import scalaz._, Scalaz._
import play.api.libs.json._
import play.api.libs.json.Json, Reads.JsObjectReducer
import play.api.libs.functional.syntax._

case class JsonPatchDoc(patches: List[JsonPatch]) {
  def apply(json: JsValue): JsResult[JsValue] = 
    patches.foldLeft(JsSuccess(json): JsResult[JsValue])((jsresult, patch) => jsresult.flatMap(patch.apply(_)))
}

sealed trait JsonPatch {
  def path: JsPatchPath
  def apply(json: JsValue): JsResult[JsValue]
}

case class JsonAdd(path: JsPatchPath, value: JsValue) extends JsonPatch {
  def apply(json: JsValue) = json.transform(__.json.update(path.json.put(value)))
}

case class JsonRemove(path: JsPatchPath) extends JsonPatch {
  def apply(json: JsValue) = json.transform(path.json.prune)
}

case class JsonReplace(path: JsPatchPath, value: JsValue) extends JsonPatch {
  def apply(json: JsValue) = json.transform(path.json.update(__.read[JsValue].map(_ => value)))
}

case class JsonMove(path: JsPatchPath, from: JsPatchPath) extends JsonPatch {
  def apply(json: JsValue) = {
    val copy = __.json.update(path.json.copyFrom(from.json.pick))
    val prune = from.json.prune
    json.transform(copy andThen prune)
  }
}

case class JsonCopy(path: JsPatchPath, from: JsPatchPath) extends JsonPatch {
  def apply(json: JsValue) = json.transform(__.json.update(path.json.copyFrom(from.json.pick)))
}

case class JsonTest(path: JsPatchPath, value: JsValue) extends JsonPatch {
  def apply(json: JsValue) = json.transform(path.json.pick).flatMap[JsValue](v => if (v == value) JsSuccess(json) 
                                                                        else JsError(path, "Test condition failed"))
}